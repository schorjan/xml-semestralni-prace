# Semestrální práce z předmětu BI-XML

Jana Schořová

1.2.2022

## Úvod

Toto je semestrální práce z předmětu BI-XML.

Dle zadání jsem si vybrala čtyři země na webové stránce The World Factbook. Jedná se o tyto země:

- Vietnam
- Iceland
- Canada
- Ireland

XML soubory jsem si dělala ručně, proto jsem nepoužila všechna data, ale jen část z nich. Jsou tam kategorie: Introduction, Geography, People and Society, Government, Communications a Transportation. Tyto soubory najdete ve složce `regions`.

Po připomínkách od vyučujícího jsem přidala ještě kategorie: Enviroment.

## Adresářová struktura

```
.
+--- .gitignore
+--- README.md
+--- info.xml    # well-formed XML soubor s informacemi o projektu
+--- regions     # složka s XML soubory
|    +--- canada.xml
|    +--- iceland.xml
|    +--- ireland.xml
|    +--- vietnam.xml
|
+--- mergeCountries.xml   # soubor pro spojení XML souborů v jeden pomocí DTD
+--- countries.xml        # výsledek spojení XML souborů
|
+--- validate    # složka se soubory na validaci
|    +--- scheme.dtd   # soubor na validaci pomocí DTD spojeného XML souboru
|    +--- scheme.rng   # soubor na validaci pomocí RelaxNG spojeného XML souboru
|
+--- xslt    #složka obsahující XSLT soubory na vytvoření HTML souborů
|    +--- index.xsl    #soubor, který pomocí XSLT vytvoří index, potřebuje spojený XML soubor
|    +--- country.xsl  #soubor, který pomocí XSLT vytvoří stránku dané země, potřebuje XML soubor země
|
+--- html    # výsledky generování HTML souborů
|    +--- css
|    |    +--- reset.css     # soubor CSS stylů, co vyresetují defaultní styly dané prohlížečem
|    |    +--- generic.css   # soubor obecných CSS stylů, které používám na všech stránkách
|    |    +--- index.css     # soubor CSS stylů, které používám v indexu
|    |    +--- country.css   # soubor CSS stylů, které používám v html souborech zemí
|    +--- index.html    # HTML soubor, který obsahuje odkazy na jednotlivé země
|    +--- ca.html       # vytvořený HTML soubor pomocí XSLT, Canada
|    +--- ic.html       # vytvořený HTML soubor pomocí XSLT, Island
|    +--- ie.html       # vytvořený HTML soubor pomocí XSLT, Ireland
|    +--- vn.html       # vytvořený HTML soubor pomocí XSLT, Vietnam
|
+--- xsl-fo
|    +--- pdf.xsl   # soubor, který pomocí XSL-FO vytvoří PDF ze sloučeného XML
|
+--- countries.pdf  # výsledný soubor generování pomocí XSL-FO
|
+--- img   # obrázky použité v HTML a v PDF
|    +--- ca/ic/ie/vn-flag.jpg
|    +--- ca/ic/ie/vn-map.jpg
|    +--- ca/ic/ie/vn-photo.jpg
```

## Použitý software

- VSCode
- XmlLint
- Saxon
- Fop

## Příkazy

### Merge

Vygeneruje XML soubor, ve kterém jsou všechny země najednou.

`xmllint --dropdtd --noent --output countries.xml mergeCountries.xml`

### Validace DTD

Zkontroluje pomocí DTD, zda vygenerovaný XML soubor ze všech XML souborů odpovídá dané struktuře.

Pokud nic nevypíše, soubor odpovídá struktuře.

`xmllint --noout --dtdvalid validate/scheme.dtd countries.xml`

### Validace RelaxNG

Zkontroluje pomocí RelaxNG, zda vygenerovaný XML soubor ze všech XML souborů odpovídá dané struktuře.

Pokud vypíše "countries.xml validates", soubor odpovídá struktuře.

`xmllint --noout --relaxng validate/scheme.rng countries.xml`

### Generování HTML souborů zemí

Vygeneruje HTML soubory pro jednotlivé země pomocí XSLT.

Canada: `java -jar saxon/saxon-he-10.5.jar -s:regions/canada.xml -xsl:xslt/country.xsl > html/ca.html`

Iceland: `java -jar saxon/saxon-he-10.5.jar -s:regions/iceland.xml -xsl:xslt/country.xsl > html/ic.html`

Ireland: `java -jar saxon/saxon-he-10.5.jar -s:regions/ireland.xml -xsl:xslt/country.xsl > html/ie.html`

Vietnam: `java -jar saxon/saxon-he-10.5.jar -s:regions/vietnam.xml -xsl:xslt/country.xsl > html/vn.html`

### Generování index.html

Vygeneruje pomocí XSLT index.html z XML souboru všech zemí.

`java -jar saxon/saxon-he-10.5.jar -s:countries.xml -xsl:xslt/index.xsl > html/index.html`

### Generování PDF

Pomocí XSL-FO souboru vygeneruje PDF soubor s informacemi o všech zemích.

`fop -xml countries.xml -xsl xsl-fo/pdf.xsl countries.pdf`
