<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/countries">
    <fo:root>
      <fo:layout-master-set>
        <fo:simple-page-master master-name="A4-portrait"
              page-height="29.7cm" page-width="21.0cm">
          <fo:region-body margin="2cm"/>
          <fo:region-after extent="2cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="A4-portrait">
        <fo:static-content flow-name="xsl-region-after">
          <fo:block text-align="center">
              <fo:page-number/>
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <fo:block-container height="100%" display-align="center" text-align="center">
            <fo:block margin-bottom="10pt">XML Semestral Work</fo:block>
            <fo:block font-size="60pt" color="#0B2545">World FactBook</fo:block>
            <fo:block font-size="20pt" margin-bottom="40pt" color="#134074">Vietnam, Iceland, Canada, Ireland</fo:block>
            <fo:block margin-bottom="5pt">Jana Schorova</fo:block>
            <fo:block>1.2.2022</fo:block>
            <!-- <xsl:value-of select="@name"/> -->
          </fo:block-container>
          <fo:block page-break-before="always"/>
          <fo:block font-size="20pt" margin-bottom="10pt">Summary</fo:block>
          <xsl:apply-templates mode="summary" />
          <xsl:apply-templates />
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template match="country" mode="summary">
    <fo:block text-align="justify" margin-top="5pt">
      <fo:block font-weight="bold" font-size="15pt" text-align-last="justify">
        <fo:basic-link internal-destination="{@id}">
          <xsl:value-of select="@name"/>
          <fo:leader leader-pattern="dots"/>
          <fo:page-number-citation ref-id="{@id}"/>
        </fo:basic-link>
      </fo:block>
    <fo:list-block>
      <xsl:for-each select="category">
        <fo:list-item>
          <fo:list-item-label>
            <fo:block></fo:block>
          </fo:list-item-label>
          <fo:list-item-body>
            <fo:block text-align-last="justify" margin-left="5pt">
              <fo:basic-link internal-destination="{generate-id(.)}">
                <xsl:value-of select="@name"/>
                <fo:leader leader-pattern="dots"/>
                <fo:page-number-citation ref-id="{generate-id(.)}"/>
              </fo:basic-link>
            </fo:block>
          </fo:list-item-body>
        </fo:list-item>
      </xsl:for-each>
    </fo:list-block>
  </fo:block>
  </xsl:template>

  <xsl:template match="country">
    <fo:block page-break-before="always"/>
    <fo:block id="{@id}">
      <fo:block margin-top="20pt" font-weight="bold" font-size="40pt" text-align="center" color="#134074">
        <xsl:value-of select="@name"/>
      </fo:block>
      <fo:block text-align="center"><!-- imgs-->
        <fo:external-graphic src="img/{@id}-flag.jpg" max-width="200px" max-height="100px" content-width="scale-to-fit" scaling="uniform" />
        &#160;
        <fo:external-graphic src="img/{@id}-map.jpg"  max-width="200px" max-height="100px" content-width="scale-to-fit" scaling="uniform" />
      </fo:block>
      <xsl:apply-templates select="category" />
      <fo:block text-align="center">
        <fo:external-graphic src="img/{@id}-photo.jpg" max-width="400px" content-width="scale-to-fit" scaling="uniform" />
      </fo:block>
    </fo:block>
  </xsl:template>

  <xsl:template match="category">
    <fo:block margin-top="25pt" id="{generate-id(.)}">
      <fo:block font-size="25pt" font-weight="bold" color="#0B2545">
        <xsl:value-of select="@name"/>
      </fo:block>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>

  <xsl:template match="section">
    <fo:block margin-top="10pt">
      <fo:block font-size="18pt" color="#13315C">
        <xsl:value-of select="@name" />
      </fo:block>
      <fo:block margin="5pt">
        <xsl:apply-templates />
      </fo:block>
    </fo:block>
  </xsl:template>

  <xsl:template match="text">
    <fo:block text-align="justify" margin="5pt">
      <xsl:value-of select="." />
    </fo:block>
  </xsl:template>

<xsl:template match="numeric-data">
  <fo:block margin="3pt">
    <fo:inline font-weight="bold">
      <xsl:value-of select="@name"/>:
    </fo:inline>
    <fo:inline>
      <xsl:value-of select="."/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="@unit"/>
    </fo:inline>
  </fo:block>
</xsl:template>

<xsl:template match="text-data">
  <fo:block margin="3pt" text-align="justify">
    <fo:inline font-weight="bold">
      <xsl:value-of select="@name"/>:
    </fo:inline>
    <fo:inline>
      <xsl:value-of select="."/>
    </fo:inline>
  </fo:block>
</xsl:template>

<xsl:template match="numeric">
  <fo:block>
    <fo:inline>
      <xsl:value-of select="."/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="@unit"/>
    </fo:inline>
  </fo:block>
</xsl:template>

<xsl:template match="data">
  <fo:block margin="3pt">
    <fo:inline font-weight="bold">
      <xsl:value-of select="@name"/>:
    </fo:inline>
    <xsl:apply-templates select="text" mode="data" />
    <xsl:apply-templates select="numeric" mode="data" />
  </fo:block>
</xsl:template>

<xsl:template match="text" mode="data">
  <fo:inline>
    <xsl:value-of select="." />
    <xsl:text> </xsl:text>
  </fo:inline>
</xsl:template>

<xsl:template match="numeric" mode="data">
  <fo:inline>
    <xsl:value-of select="." />
    <xsl:text> </xsl:text>
    <xsl:value-of select="@unit" />
  </fo:inline>
</xsl:template>
</xsl:stylesheet>