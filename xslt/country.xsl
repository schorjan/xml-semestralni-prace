<xsl:stylesheet version = '2.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output method="html" indent="yes"/>

  <xsl:template match="country">
    <html>
      <head>
        <title>
          <xsl:value-of select="@name" />
        </title>
        <link rel="stylesheet" href="./css/reset.css"/>
        <link rel="stylesheet" href="./css/generic.css"/>
        <link rel="stylesheet" href="./css/country.css"/>
      </head>
      <body>
        <header>
          <span>Semestral Work XML</span>
          <a href="index.html">BACK</a>
        </header>
        <main>
          <div class="nav">
            <ul>
              <xsl:apply-templates select="category" mode="nav"/>
            </ul>
          </div>
          <div class="content">
            <div class="intro">
              <h1>
                <xsl:value-of select="@name"/>
              </h1>
              <img src="../img/{@id}-flag.jpg" alt="flag"/>
            </div>
            <ul class="categories">
              <xsl:apply-templates select="category" />
            </ul>
            <div class="outro">
              <img src="../img/{@id}-map.jpg" alt="map"/>
              <img src="../img/{@id}-photo.jpg" alt="photo"/>
            </div>
          </div>
        </main>
        <footer>
          <p>BI-XML, FIT ČVUT</p>
          <p>1.2.2022</p>
          <p>Author: Jana Schořová</p>
        </footer>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="category" mode="nav">
    <li>
      <a href="#{@short}">
        <xsl:value-of select="@name"/>
      </a>
    </li>
  </xsl:template>

  <xsl:template match="category">
    <li id="{@short}">
      <h2>
        <xsl:value-of select="@name"/>
      </h2>
      <div class="sections">
        <xsl:apply-templates />
      </div>
    </li>
  </xsl:template>

  <xsl:template match="section">
    <section>
      <h3>
        <xsl:value-of select="@name" />
      </h3>
      <article>
        <xsl:apply-templates />
      </article>
    </section>
  </xsl:template>

  <xsl:template match="text">
    <p>
      <xsl:value-of select="." />
    </p>
  </xsl:template>

  <xsl:template match="numeric-data">
    <div class="data">
      <span class="term">
        <xsl:value-of select="@name" />
        <xsl:text>: </xsl:text>
      </span>
      <span class="value">
        <xsl:value-of select="." />
        <xsl:text> </xsl:text>
        <xsl:if test="@unit">
          <xsl:value-of select="@unit" />
        </xsl:if>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="text-data">
    <div class="data">
      <span class="term">
        <xsl:value-of select="@name" />
        <xsl:text>: </xsl:text>
      </span>
      <span class="value">
        <xsl:value-of select="." />
      </span>
    </div>
  </xsl:template>

  <xsl:template match="numeric">
    <p>
      <xsl:value-of select="." />
      <xsl:text> </xsl:text>
      <xsl:if test="@unit">
        <xsl:value-of select="@unit" />
      </xsl:if>
    </p>
  </xsl:template>

  <xsl:template match="data">
    <div class="data">
      <span class="term">
        <xsl:value-of select="@name" />
        <xsl:text>: </xsl:text>
      </span>
      <xsl:apply-templates select="text" mode="data" />
      <xsl:apply-templates select="numeric" mode="data" />
    </div>
  </xsl:template>

  <xsl:template match="text" mode="data">
    <span>
      <xsl:value-of select="." />
      <xsl:text> </xsl:text>
    </span>
  </xsl:template>

  <xsl:template match="numeric" mode="data">
    <span>
      <xsl:value-of select="." />
      <xsl:text> </xsl:text>
      <xsl:if test="@unit">
        <xsl:value-of select="@unit" />
      </xsl:if>
    </span>
  </xsl:template>
</xsl:stylesheet> 